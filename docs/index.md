# Exercices pratique de NSI

## Faire communauté :shinto_shrine:

Ces mots sont à l'origine de la naissance de notre forum. Il rassemble un nombre important d'enseignantes et d'enseignants de NSI. Les questions posées et les réponses apportées sont utiles à toutes et tous abordant de très nombreux aspects de la discipline Informatique. De l'aspect didactique, pratique, théorique, des évaluations aux projets, des différentes solutions technologiques, tous les sujets sont abordés et ces idées sont mises en commun.

Nous vous proposons ici des exercices originaux, testés, relus, critiqués et validés par les pairs. Ces exercices permettront, nous l'espérons d'entrainer vos élèves sur des sujets pouvant être proposés aux épreuves pratiques, voire d'un niveau un peu supérieur pour vos bons élèves.

## Composition d'un exercice :notebook_with_decorative_cover:

Un exercice se compose de plusieurs éléments.

* Un sujet, que nous voulons le plus original possible dans la base.
* Une fenêtre intégrée permettant de compléter directement les fonctions proposées.
* Des jeux de test, permettant de vérifier la solution par rapport aux attentes
* Une correction.
* Un lien pour télécharger les documents, afin de travailler dans un IDE préféré.
* Éventuellement d'un fichier de commentaires.

Pour plus d'information sur la façon dont vous pouvez contribuer, rendez-vous sur [Contribuer](contribuer.md)

La licence des exercices (sujets, tests, figures et commentaires) est de type Creative Commons, **sans utilisation commerciale**.

## Comment est construit ce site :hammer_and_wrench:

Ce site est rédigé en [Markdown](https://markdown.org), construit de façon statique avec [MkDocs](https://mkdocs.org) et le thème [Material](https://squidfunk.github.io/mkdocs-material/). Cette construction se fait à partir des fichiers hébergés sur la plateforme de développement [GitLab](https://about.gitlab.com) (lien vers le dépôt en bas de page).

Les logiciels qui servent à construire et publier ce site sont des [logiciels libres](https://www.april.org/articles/divers/intro_ll.html) et respectent
les 4 libertés :

1. Utiliser le logiciel pour tous les usages
2. Étudier le code source du logiciel
3. Distribuer le logiciel de façon libre
4. Améliorer le logiciel et distribuer les améliorations.



Le site fonctionne donc sans tracker de suivi ni cookies (nous ne mesurons
aucune audience). Le code JavaScript exécuté sert pour la console de
développement intégré sur les pages et nous l'hébergeons également. Seul
MathJax (utilisé pour le rendu des formules mathématiques) est servi depuis un cdn
(cdn.jsdeliver.net)
