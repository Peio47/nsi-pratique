---
author: Romain Janvier
title: Recadrer
tags:
    - 0-simple
    - 1-boucle
status: relecture
---

# Recadrer les mesures d'un tableau

Lorsqu'on utilise une sonde pour obtenir des mesures, par exemple des températures, il se peut que des mesures soient trop petites ou trop grandes. On modifie alors ces mesures pour qu'elles ne sortent pas de l'intervalle défini à l'avance.

On donne un tableau de réels `mesures` et deux réels `val_min` et `val_max`. On garantit que `val_min` est inférieur à `val_max`.

Compléter le code de la fonction `recadre` qui :

* prend `mesures`, `val_min` et `val_max` en arguments,
* modifie le tableau `mesures` de sorte que :
    * tous les éléments de `mesures` qui sont strictement inférieurs à `val_min` sont remplacés par `val_min`,
    * tous les éléments de `mesures` qui sont strictement supérieurs à `val_max` sont remplacés par `val_max`.

!!! example "Exemples"

    ```pycon
    >>> mesures_1 = [1, 2, 3, 4, 5, 6]
    >>> recadre(mesures_1, 3, 5)
    >>> mesures_1
    [3, 3, 3, 4, 5, 5]
    >>> mesures_2 = [7.1, -9.0, -3.1, 15.0, 987.7, -624.89]
    >>> recadre(mesures_2, -5.3, 15.0)
    >>> mesures_2
    [7.1, -5.3, -3.1, 15.0, 15.0, -5.3]
    ```

{{ IDE('exo') }}
