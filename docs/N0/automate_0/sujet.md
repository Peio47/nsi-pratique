---
author: Nicolas Revéret
title: Automate en dictionnaire
tags:
  - 2-tuple
  - 3-dictionnaire
---

Une *expression régulière* est une chaîne de caractères permettant de décrire d'autres chaînes de caractères.

Par exemple, la chaîne `"ba+to+"` permet de décrire les « mots » `"bato"`, `"baato"`, `"batoo"` et `"baatooooo"` : un caractère `"b"` suivi d'au moins un caractère `"a"`, puis un `"t"` et enfin un ou plusieurs caractères `"o"`.

Les expressions régulières peuvent être aussi représentées sous forme d'*automates*. Ainsi, la chaîne `"ba+to+"` est équivalente à l'automate ci-dessous :

![Automate associé à "ba+to+"](ba+to+.svg)

Dans ce graphe, chaque nœud représente un « état » de l'automate. L'état 0 est l'état de **début** et 4 celui de **fin**.

On parcourt l'automate de la façon suivante :

* on débute dans l'état de départ en lisant le premier caractère de la chaîne,
* si ce caractère est un `"b"`, on passe dans l'état 1 et on lit le caractère suivant,
* dans l'état 1, si le caractère lu est un `"a"`, on reste dans l'état 1 et on lit le caractère suivant,
* *etc*...

Si on se trouve dans l'état final après avoir lu le dernier caractère de la chaîne, alors celle-ci correspond au motif cherché.

La chaîne `"baato"` est reconnue car elle correspond au parcours 0 → 1 → 2 → 2 → 3 → 4.

On représente un automate par une série de règles stockées dans un dictionnaire Python. Chaque règle associe à un couple  `(état, caractère lu)` la valeur du prochain état.

Ainsi l'automate ci-dessus est représenté par le dictionnaire `#!py automate_0 = {(0, "b"): 1, (1, "a"): 2, (2, "a"): 2, (2, "t"): 3, (3, "o"): 4, (4, "o"): 4}`. L'état initial est le `0`, le final `4`.

On demande d'écrire les dictionnaires représentant les automates suivants. On identifiera aussi les états de départ et de fin.

=== "Cas 0"

    Cet automate reconnaît `"bato"` et `"baato"` mais pas "bota" (exemple du sujet) :

    ![Automate associé à ba+to+"](ba+to+.svg)

=== "Cas 1"

    Cet automate reconnaît `"tada"` et `"tadaaa"` mais pas `"tad"` ni `"taada"` :

    ![Automate associé à "tada+"](tada+.svg)

=== "Cas 2"

    Cet automate reconnaît `"ruche"` et `"riche"` mais pas `"reche"` ni `"rche"` :

    ![Automate associé à "r(u|i)che"](ruiche.svg)

=== " Cas 3"

    Cet automate reconnaît `"vroum"`, `"broum"` et `"vrououm"` mais pas `"brou"` ni `"vrouom"`.

    ![Automate associé à "(v|b)r(ou)+m"](vbrououm.svg)

{{ IDE('exo') }}
