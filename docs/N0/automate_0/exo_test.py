# Tests
# Cet automate reconnaît "bato" et "baato" mais pas "bota" (exemple du sujet)
automate_0_corr = {
    (0, "b"): 1,
    (1, "a"): 2,
    (2, "a"): 2,
    (2, "t"): 3,
    (3, "o"): 4,
    (4, "o"): 4,
}
debut_0_corr = 0
fin_0_corr = 4
assert automate_0 == automate_0_corr
assert debut_0 == debut_0_corr
assert fin_0 == fin_0_corr

# Cet automate reconnaît "tada" et "tadaaa" mais pas "tad" ni "taada"
automate_1_corr = {(0, "t"): 1, (1, "a"): 2, (2, "d"): 3, (3, "a"): 3, (4, "a"): 4}
debut_1_corr = 0
fin_1_corr = 4
assert automate_1 == automate_1_corr
assert debut_1 == debut_1_corr
assert fin_1 == fin_1_corr

# Cet automate reconnaît "ruche" et "riche" mais pas "reche" ni "rche"
automate_2_corr = {
    (0, "r"): 1,
    (1, "u"): 2,
    (2, "c"): 4,
    (1, "i"): 3,
    (3, "c"): 4,
    (4, "h"): 5,
    (5, "e"): 6,
}
debut_2_corr = 0
fin_2_corr = 6
assert automate_2 == automate_2_corr
assert debut_2 == debut_2_corr
assert fin_2 == fin_2_corr

# Cet automate reconnaît `"vroum"`, `"broum"` et `"vrououm"` mais pas `"brou"` ni `"vrouom"`
automate_3_corr = {
    (0, "v"): 1,
    (0, "b"): 2,
    (1, "r"): 3,
    (2, "r"): 3,
    (3, "o"): 4,
    (4, "u"): 5,
    (5, "o"): 4,
    (5, "m"): 6,
}
debut_3_corr = 0
fin_3_corr = 6
assert automate_3 == automate_3_corr
assert debut_3 == debut_3_corr
assert fin_3 == fin_3_corr
