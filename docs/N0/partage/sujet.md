---
author: Nicolas Revéret
title: Partage
tags:
    - 0-simple
    - 1-boucle
status: relecture
---

# Partage d'un tableau

On donne une liste Python `valeurs` et un entier `effectif_gauche`.

On garantit que `effectif_gauche` est un entier entre 0 et la longueur de `valeurs` (inclus l'un et l'autre).

Compléter le code de la fonction `partage` qui :

* prend `valeurs` et `effectif_gauche` en arguments,
* renvoie le couple formé :
    * de la liste comprenant les `effectif_gauche` premiers éléments de `valeurs` (situés à gauche dans `valeurs`),
    * de la liste comprenant les éléments restants (situés à droite).

!!! example "Exemples"

    ```pycon
    >>> partage(['pim', 'pam', 'poum'], 2)
    (['pim', 'pam'], ['poum'])
    >>> partage([7, 12, 5, 6, 8], 0)
    ([], [7, 12, 5, 6, 8])
    >>> partage([7, 12, 5, 6, 8], 5)
    ([7, 12, 5, 6, 8], [])
    ```

{{ IDE('exo') }}
