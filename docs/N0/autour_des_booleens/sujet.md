---
author: Nicolas Revéret
title: Autour des booléens
tags:
  - 2-booléen
---
# Autour des booléens

## Présentation

Un objet de type `bool` ne peut avoir que l'une des deux valeurs `#!py True` ou `#!py False` appelées **valeurs booléennes**. Ces valeurs permettent de préciser si une affirmation est vraie ou fausse.

Par exemple, l'affirmation « *3 est strictement inférieur à 4* » est vraie. Traduite en Python, elle devient `3 < 4` et est évaluée à `#!py True`.

À l'inverse, l'affirmation « *le caractère "a" est présent dans le mot "chien"* » est fausse : l'expression `"a" in "chien"` est évaluée par Python à `#!py False`.

L'adjectif « *booléen* » est un hommage au mathématicien anglais George Boole (1815 - 1864).

On donne ci-dessous des exemples d'expressions renvoyant des booléens :

???+ example "Exemples"

    * Comparaisons :

    ```pycon
    >>> 1 + 1 == 2 # test d'égalité
    True
    >>> 1 + 1 != 3 # test de différence
    True
    >>> 1 >= 1
    True
    >>> 'allo' < 'bonjour'
    True
    ```

    * Divisibilité de nombres :

    ```pycon
    >>> # 6 est-il pair ?
    >>> # 6 est-il divisible par 2 ?
    >>> # Le reste de la division de 6 par 2 vaut-il 0 ?
    >>> 6 % 2 == 0
    True
    >>> # 6 est-il impair ?
    >>> # Le reste de la division de 6 par 2 vaut-il 1 ?
    >>> 6 % 2 == 1
    False
    ```

    * Appartenance à une chaîne de caractère, une liste... :

    ```pycon
    >>> "a" in "allo"
    True
    >>> 5 not in [3, 6, 9, 12, 15]
    True
    ```

    * Opérateurs booléens

    ```pycon
    >>> ('a' in 'allo') and (1 + 1 == 3)
    False
    >>> ('b' in 'allo') or (1 + 1 == 3)
    False
    ```

    *Remarque :* Les parenthèses autour de chaque test ne sont pas toujours nécessaires. On les ajoute ici pour des raisons de lisibilité.

## Au travail

Compléter le code ci-dessous en saisissant les tests demandés **et non leur résultat**.

Afin de corriger les propositions, le résultat de chaque test est affecté à une variable (`test_1`, `test_2`...). Ce sont les valeurs de ces variables qui sont vérifiées dans la correction automatique. 

{{ IDE('exo') }}