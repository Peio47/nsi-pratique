# Tests
# 3 est-il strictement inférieur à 4 ?
assert test_1, "Erreur avec : 3 est-il strictement inférieur à 4 ?"
# 3 est-il supérieur ou égal à 4 ?
assert not test_2, "Erreur avec : 3 est-il strictement inférieur à 4 ?"
# le caractère 'n' est-il dans la chaîne 'bonjour' ?
assert test_3, "Erreur avec : le caractère 'n' est-il dans la chaîne 'bonjour' ?"
# Le calcul 3 + 5 * 2 vaut-il 16 ?
assert not test_4, "Erreur avec : Le calcul 3 + 5 * 2 vaut-il 16 ?"
# 5 est-il un nombre pair ?
assert not test_5, "Erreur avec : 5 est-il un nombre pair ?"
# 12 est-il dans la liste [k for k in range(12)] ?
assert not test_6, "Erreur avec : 12 est-il dans la liste [k for k in range(12)] ?"
# 'ju' n'est-il pas dans 'bonjour' ?
assert test_7, "Erreur avec : 'ju' n'est-il pas dans 'bonjour' ?"
# Est-ce que 3 est égal à 1 + 2 et 'a' est dans 'Boole' ?
assert (
    not test_8
), "Erreur avec : Est-ce que 3 est égal à 1 + 2 et 'a' est dans 'Boole' ?"
# Est-ce que 3 est égal à 1 + 2 ou 'a' est dans 'Boole' ?
assert test_9, "Erreur avec : # Est-ce que 3 est égal à 1 + 2 ou 'a' est dans 'Boole' ?"
# 6 est-il un nombre pair et un multiple de 3 ?
assert test_10, "Erreur avec : 6 est-il un nombre pair et un multiple de 3 ?"
# 648 est-il dans la table de 2, de 3 et de 4 ?
assert test_11, "Erreur avec : 648 est-il dans la table de 2, de 3 et de 4 ?"
# 25 est-il compris entre 24 et 26 (au sens strict) ?
assert test_12, "Erreur avec : 25 est-il compris entre 24 et 26 (au sens strict) ?"
# 'a' est-il dans 'hello' ou dans 'hallo' ?
assert test_13, "Erreur avec : 'a' est-il dans 'hello' ou dans 'hallo' ?"
# 8 est-il égal à 4 * 2 et différent de 9 - 1 ?
assert not test_14, "Erreur avec : 8 est-il égal à 4 * 2 et différent de 9 - 1 ?"
# 7 est-il compris entre 1 et 5 (au sens large) ou strictement supérieur à 6 ?
assert (
    test_15
), "Erreur avec : 7 est-il compris entre 1 et 5 (au sens large) ou strictement supérieur à 6 ?"
