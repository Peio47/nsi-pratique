def manhattan(xA, yA, xB, yB):
    return abs(xB - xA) + abs(yB - yA)


def euclidienne_carre(xA, yA, xB, yB):
    return (xB - xA) ** 2 + (yB - yA) ** 2


def tchebychev(xA, yA, xB, yB):
    return max(abs(xB - xA), abs(yB - yA))


# Tests
xA, yA = 2, -5
xB, yB = -3, 1
assert manhattan(xA, yA, xB, yB) == 11
assert euclidienne_carre(xA, yA, xB, yB) == 61
assert tchebychev(xA, yA, xB, yB) == 6

xA, yA = 2, 2
xB, yB = 2, 2
assert manhattan(xA, yA, xB, yB) == 0
assert euclidienne_carre(xA, yA, xB, yB) == 0
assert tchebychev(xA, yA, xB, yB) == 0
