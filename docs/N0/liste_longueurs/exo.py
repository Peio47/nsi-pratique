def filtre_par_longueur(liste_prenoms, ...):
    resultat = []
    for prenom in ...:
        if ... == longueur:
            resultat.append(...)
    return ...


# Tests
prenoms = ['Anne', 'Francky', 'Charles', 'Léa', 'Nicolas']
assert filtre_par_longueur(prenoms, 7) == ['Francky', 'Charles', 'Nicolas']
assert filtre_par_longueur(prenoms, 3) == ['Léa']
assert filtre_par_longueur(prenoms, 10) == []
