class Chien:
    def __init__(self, age, poids):
        self.... = age
        ...

    def age_humain(self):
        return ...

    def age_chien(self):
        if self.age == ...:
            return 0
        elif ...:
            return ...
        else:
            ...

    def machouille(self, jouet):
        resultat = ""
        for i in range(...):
            ... += ...[...]
        return ...

    def aboie(self, nombre):
        ...

    def veillit(self):
        if self.age ...:
            self.age = ...
            return ...
        else:
            ...

    def mange(self, ration):
        if ... < ration <= ...:
            self.poids = ...
            return ...
        else:
            return ...


# Tests
medor = Chien(1, 12.0)
assert medor.age_humain() == 1
assert medor.age_chien() == 20
assert medor.poids == 12
assert medor.machouille("bâton") == "bâto"
assert medor.aboie(3) == "OuafOuafOuaf"
assert medor.veillit() == True
assert medor.age_chien() == 28
assert medor.mange(2.0) == False
assert medor.mange(1.0) == True
assert medor.poids == 12.125
assert medor.mange(1.2125) == True
