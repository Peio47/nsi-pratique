class Chien:
    def __init__(self, age, poids):
        self.age = age
        self.poids = poids

    def age_humain(self):
        return self.age

    def age_chien(self):
        if self.age == 0:
            return 0
        elif self.age == 1:
            return 20
        else:
            return 20 + 4 * self.age

    def machouille(self, jouet):
        resultat = ""
        for i in range(len(jouet) - 1):
            resultat += jouet[i]
        return resultat

    def aboie(self, nombre):
        return "Ouaf" * nombre

    def veillit(self):
        if self.age < 20:
            self.age += 1
            return True
        else:
            return False

    def mange(self, ration):
        if 0 < ration <= self.poids / 10:
            self.poids += 0.125 * ration
            return True
        else:
            return False


# Tests
medor = Chien(1, 12.0)
assert medor.age_humain() == 1
assert medor.age_chien() == 20
assert medor.poids == 12.0
assert medor.machouille("bâton") == "bâto"
assert medor.aboie(3) == "OuafOuafOuaf"
assert medor.veillit() == True
assert medor.age_chien() == 28
assert medor.mange(2.0) == False
assert medor.mange(1.0) == True
assert medor.poids == 12.125
assert medor.mange(1.2125) == True
