def perm_rec(liste, pris):
    if len(pris) == ...:
        return [pris.copy()]
    else:
        liste_permutations = ...
        for element in liste:
            if ...:
                pris.append(...)
                for permut in perm_rec(..., ...):
                    liste_permutations.append(...)
                pris.pop()
        return ...

def permutations(liste):
    return perm_rec(..., ...)

# tests

assert perm_rec([1, 2, 3], [1]) == [[1, 2, 3], [1, 3, 2]]
assert perm_rec(['a', 'v', 'i', 'o', 'n'], ['n', 'i', 'v']) == [['n', 'i', 'v', 'a', 'o'], ['n', 'i', 'v', 'o', 'a']]
assert perm_rec([7, 4, 5, 3, 8], [3, 4]) == [[3, 4, 7, 5, 8], [3, 4, 7, 8, 5], [3, 4, 5, 7, 8], [3, 4, 5, 8, 7], [3, 4, 8, 7, 5], [3, 4, 8, 5, 7]]
assert perm_rec([6, 1, 4], []) == [[6, 1, 4], [6, 4, 1], [1, 6, 4], [1, 4, 6], [4, 6, 1], [4, 1, 6]]

assert permutations([1, 2, 3]) == [[1, 2, 3], [1, 3, 2], [2, 1, 3], [2, 3, 1], [3, 1, 2], [3, 2, 1]]
assert permutations([]) == [[]]
assert permutations([6, 1, 4]) == [[6, 1, 4], [6, 4, 1], [1, 6, 4], [1, 4, 6], [4, 6, 1], [4, 1, 6]]
