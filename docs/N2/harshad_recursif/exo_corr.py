def somme_chiffres(n):
    if n == 0:
        return 0
    else:
        return n % 10 + somme_chiffres(n//10)


def harshad(n):
    assert n > 0
    return n % somme_chiffres(n) == 0


# Tests
assert somme_chiffres(8) == 8
assert somme_chiffres(18) == 9
assert somme_chiffres(409) == 13
assert harshad(18)
assert harshad(72)
assert not harshad(11)
