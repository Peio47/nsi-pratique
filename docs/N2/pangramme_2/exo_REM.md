# Commentaires

{{ IDE('exo_corr') }}

La fonction `indice` permet de mettre en correspondance chaque lettre avec un indice dans le tableau.

On utilise pour cela le code ASCII de chaque lettre. Dans la table ASCII, les lettres latines en minuscule sont à la suite et dans l'ordre alphabétique. Ainsi `#!py ord("b") - ord("a")` renvoie `1` ce qui permet d'accéder à la bonne case du tableau.

La fonction réciproque `minuscule` utilise `#!py chr`.

## Avec un ensemble

La structure d'ensemble de Python est aussi adaptée ici :

```python
def lettres_manquantes(phrase):
    lettres = {minuscule(i) for i in range(26)}
    for caractere in phrase:
        lettres.discard(caractere)
    return sorted(list(lettres))
```

Il faut toutefois être attentif à deux points :

* la méthode `remove` d'un ensemble Python lève une erreur si la valeur à retirer n'est pas présente. Ce n'est pas le cas de `discard` ;
* les éléments d'un ensemble ne sont pas triés. On doit renvoyer le résultat **trié** de la conversion : `sorted(list(lettres))`.