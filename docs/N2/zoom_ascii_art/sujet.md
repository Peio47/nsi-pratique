---
author: BNS2022-39.2 puis Sébastien HOARAU
title: Zoom matrice image
tags:
    - grille
--- 

![image](img39_2a.png){: .center width=30%}

On travaille sur des dessins en noir et blanc obtenu à partir de pixels noirs et blancs :
La figure « cœur » ci-dessus va servir d’exemple.
On la représente par une grille de nombres, c’est-à-dire par une liste composée de sous-listes de même longueur.
Chaque sous-liste représentera donc une ligne du dessin.

Dans le code ci-dessous, la fonction `affiche` permet d’afficher le dessin. Les pixels noirs (1 dans la grille) seront représentés par le caractère "*" et les blancs (0 dans la grille) par deux espaces.

La fonction `zoom_liste` prend en argument une liste `liste_depart` et un entier `k`. Elle
renvoie une liste où chaque élément de `liste_depart` est dupliqué `k` fois.

La fonction `zoom_dessin` prend en argument la grille `dessin` et renvoie une grille où
toutes les lignes de `dessin` sont zoomées `k` fois et répétées `k` fois.

Compléter le code dans l'IDE pour obtenir les résultats ci-dessous :

!!! example "Exemples"

    ```pycon
    >>> coeur = [[0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 1, 1, 0, 0, 0, 1, 1, 0, 0, 0],
        [0, 0, 1, 0, 0, 1, 0, 1, 0, 0, 1, 0, 0],
        [0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0],
        [0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0],
        [0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0],
        [0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0],
        [0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 0, 0, 0],
        [0, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]]

    >>> affiche(coeur)

        * *       * *      
      *     *   *     *    
    *         *         *  
    *                   *  
    *                   *  
      *               *    
        *           *      
          *       *        
            *   *          
              *            
                            
    >>> affiche(zoom_dessin(coeur,3))

                    * * * * * *                   * * * * * *                  
                    * * * * * *                   * * * * * *                  
                    * * * * * *                   * * * * * *                  
              * * *             * * *       * * *             * * *            
              * * *             * * *       * * *             * * *            
              * * *             * * *       * * *             * * *            
        * * *                         * * *                         * * *      
        * * *                         * * *                         * * *      
        * * *                         * * *                         * * *      
        * * *                                                       * * *      
        * * *                                                       * * *      
        * * *                                                       * * *      
        * * *                                                       * * *      
        * * *                                                       * * *      
        * * *                                                       * * *      
              * * *                                           * * *            
              * * *                                           * * *            
              * * *                                           * * *            
                    * * *                               * * *                  
                    * * *                               * * *                  
                    * * *                               * * *                  
                          * * *                   * * *                        
                          * * *                   * * *                        
                          * * *                   * * *                        
                                * * *       * * *                              
                                * * *       * * *                              
                                * * *       * * *                              
                                      * * *                                    
                                      * * *                                    
                                      * * *                                    
    ```

{{ IDE('exo') }}

