# Tests
assert compression_RLE("aabbbbcaa") == "2a4b1c2a"
assert compression_RLE("aa aa") == "2a1 2a"
assert compression_RLE("aaa") == "3a"
assert compression_RLE("aA") == "1a1A"


# Tests supplémentaires
assert compression_RLE("r") == "1r"
assert compression_RLE("r"*10) == "10r"
assert compression_RLE("r"*10+"a"*5) == "10r5a"