def compression_RLE(texte):
    chaine_compressee = ""

    caractere_repete = texte[0]
    nb_repetitions = ...

    for caractere in texte:
        if caractere == ...:
            nb_repetitions = ...
        else:
            chaine_compressee += ... + ...
            caractere_repete = ...
            nb_repetitions = ...

    chaine_compressee += ...

    return ...


# Tests
assert compression_RLE("aabbbbcaa") == "2a4b1c2a"
assert compression_RLE("aa aa") == "2a1 2a"
assert compression_RLE("aaa") == "3a"
assert compression_RLE("aA") == "1a1A"
