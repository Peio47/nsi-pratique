VALEURS =  {'A': 1, 'B': 3, 'C': 3, 'D': 2, 'E': 1, 'F': 4, 'G': 2, 'H': 4, 
            'I': 1, 'J': 8, 'K': 10, 'L': 1, 'M': 2, 'N': 1, 'O': 1, 'P': 3, 
            'Q': 8, 'R': 1, 'S': 1, 'T': 1, 'U': 1, 'V': 4, 'W': 10, 'X': 10, 
            'Y': 10, 'Z': 10}


# corrigé V1

LETTRE_X2 ='+'
LETTRE_X3 ='*'
MOT_X2 = '#'
MOT_X3 = "@"

def calcul_score(mot, masque):
    score = 0
    coeff = 1
    for i in range(len(mot)):
        if masque[i] == LETTRE_X2:
            score += 2 * VALEURS[mot[i]]
        elif masque[i] == LETTRE_X3:
            score += 3 * VALEURS[mot[i]]
        else:
            score += VALEURS[mot[i]]
            if masque[i] == MOT_X2:
                coeff = coeff*2
            if masque[i] == MOT_X3:
                coeff = coeff*3
    return score * coeff

# corrigé V2

BONUS_LETTRE = {'+': 2, '*': 3}
BONUS_MOT = {'#': 2, '@': 3}

def calcul_score(mot, masque):
    score = 0
    coeff_mot = 1
    for i in range(len(mot)):
        coeff_lettre = 1
        lettre, symbole_bonus = mot[i], masque[i]
        if symbole_bonus in BONUS_LETTRE:
            coeff_lettre = coeff_lettre * BONUS_LETTRE[symbole_bonus]
        elif symbole_bonus in BONUS_MOT:
            coeff_mot = coeff_mot * BONUS_MOT[symbole_bonus]
        score += VALEURS[lettre] * coeff_lettre
    return score * coeff_mot

# tests
assert calcul_score("GIRAFE","-+---#") == 22
assert calcul_score("KAYAK", "--+--") == 42
assert calcul_score("INFORMATIQUE", "--@---------") == 69




