# tests

adresse_enonce = AdresseIP('192.168.0.1', '255.224.0.0') 
assert adresse_enonce.plage_adresses() == ([192, 160, 0, 0], [192, 191, 255, 255])

# Autres tests

adr_1 = AdresseIP('10.45.185.24', '255.255.248.0')
assert adr_1.plage_adresses() == ([10, 45, 184, 0], [10, 45, 191, 255])
