# Tests
# Nombres usuels
zero = Entier()

un = Entier()
un.incremente()
un_bis = un.copie()

deux = Entier()
deux.incremente()
deux.incremente()
deux_bis = deux.copie()

trois = Entier()
trois.incremente()
trois.incremente()
trois.incremente()
trois_bis = trois.copie()

cinq = Entier()
cinq.incremente()
cinq.incremente()
cinq.incremente()
cinq.incremente()
cinq.incremente()
cinq_bis = cinq.copie()

# Additions
assert zero + zero == zero

assert un + deux == trois
assert un == un_bis and deux == deux_bis, "Les objets ne doivent pas être modifiés"

# Différences
assert zero - zero == zero

assert deux - un == un
assert un == un_bis and deux == deux_bis, "Les objets ne doivent pas être modifiés"

assert un - deux == "Opération impossible"

# Tests supplémentaires
nombres = [Entier()]
for k in range(1, 20):
    n = nombres[-1].copie()
    n.incremente()
    nombres.append(n)
for i in range(10):
    a = nombres[i]
    b = nombres[i + 1]
    assert a + b == nombres[2 * i + 1], f"Erreur sur une addtion"
    assert b - a == nombres[1], f"Erreur sur une soustraction"
    assert a - b == "Opération impossible", f"Erreur sur une soustraction impossible"
