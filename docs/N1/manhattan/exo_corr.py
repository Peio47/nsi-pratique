def distance(xA, yA, xB, yB):
    return abs(xB-xA)+abs(yB-yA)


def prochaine_livraison(livraisons, pos_x, pos_y):
    prochain_x = livraisons[0][0]
    prochain_y = livraisons[0][1]
    distance_minimale = distance(pos_x, pos_y, prochain_x, prochain_y)
    for i in range(1, len(livraisons)):
        x_i, y_i = livraisons[i]
        distance_i = distance(pos_x, pos_y, x_i, y_i)
        if distance_i < distance_minimale:
            prochain_x = x_i
            prochain_y = y_i
            distance_minimale = distance_i
    return prochain_x, prochain_y


# Tests
assert distance(0, 1, 2, 3) == 4
adresses = [(2, 0), (0, 1), (3, 3), (2, 3)]
assert prochaine_livraison(adresses, 0, 0) == (0, 1)
adresses = [(2, 0), (3, 3), (2, 3)]
assert prochaine_livraison(adresses, 0, 1) == (2, 0)
adresses = [(3, 3), (2, 3)]
assert prochaine_livraison(adresses, 2, 0) == (2, 3)
adresses = [(3, 3)]
assert prochaine_livraison(adresses, 2, 3) == (3, 3)
adresses = [(1, 0), (0, 1)]
assert prochaine_livraison(adresses, 0, 0) == (1, 0)
