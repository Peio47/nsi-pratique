def rendu(somme_a_rendre):
    n_5 = somme_a_rendre // 5
    somme_a_rendre -= 5 * n_5

    n_2 = somme_a_rendre // 2
    somme_a_rendre -= 2 * n_2

    n_1 = somme_a_rendre
    
    return (n_5, n_2, n_1)
