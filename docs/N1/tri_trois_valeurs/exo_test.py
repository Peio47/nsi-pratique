# Tests
nombres = [2, 0, 2, 1, 2, 1]
tri_3_valeurs(nombres)
assert nombres == [0, 1, 1, 2, 2, 2]

nombres = [1, 1, 0, 1]
tri_3_valeurs(nombres)
assert nombres == [0, 1, 1, 1]

# Tests supplémentaires
from random import shuffle

nombres = [0] * 10
tri_3_valeurs(nombres)
assert nombres == [0] * 10

nombres = [1] * 10
tri_3_valeurs(nombres)
assert nombres == [1] * 10

nombres = [2] * 10
tri_3_valeurs(nombres)
assert nombres == [2] * 10

nombres = [0] * 10 + [2] * 10
shuffle(nombres)
tri_3_valeurs(nombres)
assert nombres == [0] * 10 + [2] * 10

nombres = [0] * 10 + [1] * 10 + [2] * 10
shuffle(nombres)
tri_3_valeurs(nombres)
assert nombres == [0] * 10 + [1] * 10 + [2] * 10
