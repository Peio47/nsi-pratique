---
author: Nicolas Revéret
title: Mise en boîtes
tags:
    - 4-grille
status: relecture
---

# Mise en boîte

> On n'utilisera pas `sum` dans cet exercice.

On souhaite ranger différents objets dont le poids est connu dans des boîtes dont la capacité totale ne peut pas dépasser une certaine valeur.

Les poids sont fournis dans une liste triée dans l'ordre décroissant. Il sont tous exprimés en kilogrammes.

Toutes les boîtes ont la même contenance maximale et l'on garantit que celle-ci est supérieure ou égale au poids de l'objet le plus lourd.

???+ example "Exemple (1)"

    ```python
    poids_objets = [7, 5, 3, 1]
    poids_max = 8
    ```

    Dans cet exemple, on a quatre objets à ranger (de poids 7, 5, 3 et 1 kg) et les boîtes ne peuvent pas peser plus de 8 kg (inclus).

On appelle « rangement valide » la donnée d'une liste Python dans laquelle chaque élément est une liste contenant les poids de certains objets :

* chaque poids doit apparaître une unique fois dans la liste,
* le poids total de chaque sous-liste doit être inférieur ou égal à la contenance maximale des boîtes.

???+ example "Exemple (2)"

    La liste `#!py [[7, 1], [5, 3]]` est un rangement valide. On a placé les objets de poids 7 et 1 dans une boîte et les deux autres dans une seconde boîte.

    La liste `#!py [[7, 5], [3, 1]]` n'est **pas** un rangement valide : le poids total de la première boîte est trop grand.

Pour ranger les objets, on **impose** la méthode suivante :

* on crée une liste vide `boites` ;
* on parcourt les objets dans l'ordre décroissant (on garantit que la liste `poids_objets` est ainsi triée) :
    * pour chacun d'entre eux, on parcourt l'ensemble des boîtes et l'on se demande pour chacune si l'objet peut y être ajouté :
        * si c'est le cas, on ajoute cet objet,
        * sinon, on passe à la boîte suivante,
* si l'on a atteint la dernière boîte sans avoir rangé l'objet, on l'ajoute dans une nouvelle boîte à la fin de la liste `boites`.

???+ example "Exemple (3)"

    On applique l'algorithme dans le cas suivant :

    ```python
        poids_objets = [7, 5, 3, 1]
        poids_max = 8
    ```

    === "État initial"

        La liste des boîtes est vide :

        `#!py boites = []`

    === "Objet de poids `7`"

        On ajoute une nouvelle boîte pour cet objet :

        `#!py boites = [[7]]`

    === "Objet de poids `5`"

        On ajoute une nouvelle boîte pour cet objet :

        `#!py boites = [[7] , [5]]`

    === "Objet de poids `3`"

        Cet objet tient dans la deuxième boîte :

        `#!py boites = [[7] , [5, 3]]`

    === "Objet de poids `1`"

        Cet objet tient dans la première boîte :

        `#!py boites = [[7, 1] , [5, 3]]`

Vous devez écrire deux fonctions :

* `poids_boite` prend en argument une liste de nombres entiers (une boîte) et renvoie la somme de ces nombres (le poids de la boîte),
* `rangement` prend en arguments la liste des poids des objets `poids_objets` ainsi que le poids maximal que peut recevoir une boîte `poids_max` et renvoie la liste **non triée** des boîtes utilisées en appliquant la méthode décrite

!!! attention "Attention"

    Comme indiqué ci-dessus, **on ne triera ni les objets ni les boîtes** au risque de faire échouer les tests.

!!! example "Exemples"

    ```pycon
    >>> poids_boite([])
    0
    >>> poids_boite([7, 5, 3, 1])
    16
    >>> rangement([7, 5, 3, 1], 8)
    [[7, 1], [5, 3]]
    >>> rangement([7, 5, 3, 1], 16)
    [[7, 5, 3, 1]]
    ```

{{ IDE('exo') }}
