from random import randrange, shuffle
import drawSvg as draw


def est_drapeau(nombres):
    return all([nombres[i] <= nombres[i + 1] for i in range(len(nombres) - 1)])


def drapeau(nombres, pivot):
    global d
    debut = 0
    milieu = 0
    fin = len(nombres) - 1

    num_figure = 1
    while milieu <= fin:
        if nombres[milieu] < pivot:
            nombres[milieu], nombres[debut] = nombres[debut], nombres[milieu]
            debut += 1
            milieu += 1
        elif nombres[milieu] == pivot:
            milieu += 1
        else:
            nombres[milieu], nombres[fin] = nombres[fin], nombres[milieu]
            fin -= 1
        dessin(nombres, debut, milieu, fin, num_figure)
        num_figure += 1


def dessin(nombres, i_zeros, i_uns, i_deux, num_fichier):
    w, h = (taille + 2) * TAILLE_CASE, 4 * TAILLE_CASE
    d = draw.Drawing(w, h, origin=(0, 0))
    # d.append(draw.Rectangle(0, 0, w, h, fill="#ffffff"))

    for i, valeur in enumerate(nombres):
        couleur = GRIS
        if i < i_zeros:
            couleur = BLEU
        elif i < i_uns:
            couleur = BLANC
        elif i > i_deux:
            couleur = ROUGE
        couleur_bord = BORD
        case = draw.Rectangle(
            (i + 1) * TAILLE_CASE,
            TAILLE_CASE,
            TAILLE_CASE,
            TAILLE_CASE,
            fill=couleur,
            strokewidth=1,
            stroke=couleur_bord,
        )
        d.append(case)

        numero = draw.Text(
            str(valeur),
            TAILLE_POLICE,
            (i + 1.5) * TAILLE_CASE,
            1.5 * TAILLE_CASE,
            text_anchor="middle",
            valign="middle",
        )
        d.append(numero)

    texte_zero = draw.Text(
        "debut",
        TAILLE_POLICE_INDICE,
        (i_zeros + 1.5) * TAILLE_CASE,
        0.5 * TAILLE_CASE,
        text_anchor="middle",
        valign="middle",
    )
    d.append(texte_zero)

    texte_uns = draw.Text(
        "milieu",
        TAILLE_POLICE_INDICE,
        (i_uns + 1.5) * TAILLE_CASE,
        2.75 * TAILLE_CASE,
        text_anchor="middle",
        valign="middle",
    )
    d.append(texte_uns)

    texte_deux = draw.Text(
        "fin",
        TAILLE_POLICE_INDICE,
        (i_deux + 1.5) * TAILLE_CASE,
        3.4 * TAILLE_CASE,
        text_anchor="middle",
        valign="middle",
    )
    d.append(texte_deux)

    d.saveSvg(f"bis_etape_{num_fichier}.svg")


TAILLE_CASE = 10
TAILLE_POLICE = 8
TAILLE_POLICE_INDICE = 7
BLEU = "#4483d6"
BLANC = "#ffffff"
ROUGE = "#f42411"
GRIS = "#888"
BORD = "#3a3a3a"

taille = 6
nb_bleu = randrange(1, taille // 2)
nb_blanc = randrange(1, taille // 2)
nb_rouge = taille - nb_bleu - nb_blanc
nombres = [0] * nb_bleu + [1] * nb_blanc + [2] * nb_rouge
shuffle(nombres)

nombres = [3, 0, 2, 1, 5, 1]
d = dessin(nombres, 0, 0, taille - 1, 0)
drapeau(nombres, 1)


"""
vrai = []

for _ in range(20):
    taille = 1000
    nb_bleu = randrange(1, taille // 2)
    nb_blanc = randrange(1, taille // 2)
    nb_rouge = taille - nb_bleu - nb_blanc
    billes = [0] * nb_bleu + [1] * nb_blanc + [2] * nb_rouge
    shuffle(billes)
    drapeau(billes)
    vrai.append(est_drapeau(billes))
print(all(vrai))
"""
