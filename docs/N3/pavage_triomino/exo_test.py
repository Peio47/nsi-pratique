# tests

assert est_pavage(2, 1, 1, [(0, 0, 3)]) == True

assert est_pavage(3, 1, 2, [(0, 0, 3), (2, 1, 0)]) == False

triominos = [(0, 1, 2), (2, 1, 2), (0, 2, 3), (2, 3, 0), (3, 2, 1)]
assert est_pavage(4, 1, 0, triominos) == False


triominos = [
    (0, 0, 3), (0, 2, 3), (1, 4, 0), (2, 1, 1),
    (2, 3, 3), (3, 0, 1), (4, 2, 0), (4, 4, 0)
]
assert est_pavage(5, 4, 0, triominos) == True

# autres tests

assert est_pavage(2, 0, 0, [(0, 0, 3)]) == False
assert est_pavage(2, 0, 0, [(0, 1, 2)]) == False

triominos = [
    (0, 0, 3), (0, 0, 3), (0, 2, 3), (1, 4, 0), (2, 1, 1),
    (2, 3, 3), (3, 0, 1), (4, 2, 0), (4, 4, 0)
]
assert est_pavage(5, 4, 0, triominos) == False, "Il y a un doublon"

triominos = [
    (0, 2, 3), (1, 4, 0), (2, 1, 1),
    (2, 3, 3), (3, 0, 1), (4, 2, 0), (4, 4, 0)
]
assert est_pavage(5, 4, 0, triominos) == False, "Il y en manque"


assert est_pavage(4, 2, 1, [(1, 0, 1), (0, 2, 0), (1, 3, 0), (3, 0, 1), (2, 3, 2)]) == False, "Dessiner ce cas !!!"

