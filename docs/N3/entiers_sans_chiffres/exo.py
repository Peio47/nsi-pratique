#--- HDR --- #
def successeur(n):
    return (n,)


ZERO = tuple()
UN = successeur(ZERO)
DEUX = successeur(UN)
TROIS = successeur(DEUX)
QUATRE = successeur(TROIS)
CINQ = successeur(QUATRE)
SIX = successeur(CINQ)
SEPT = successeur(SIX)
HUIT = successeur(SEPT)
#--- HDR --- #


def addition(a, b):
    ...


def multiplication(a, b):
    ...


def soustraction(a, b):
    ...


# Tests
# un + un == deux
assert addition(UN, UN) == DEUX
# trois + cinq == huit
assert addition(TROIS, DEUX) == CINQ
# zéro * cinq == zéro
assert multiplication(ZERO, CINQ) == ZERO
# deux * trois == six
assert multiplication(DEUX, TROIS) == SIX
# cinq - trois  == deux
assert soustraction(CINQ, TROIS) == DEUX
# trois - cinq -> Opération impossible
try:
    soustraction(DEUX, CINQ)
except ValueError:
    pass
