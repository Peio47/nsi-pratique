#--- HDR --- #ajoute_un
def successeur(n):
    return (n,)


ZERO = tuple()
UN = successeur(ZERO)
DEUX = successeur(UN)
TROIS = successeur(DEUX)
QUATRE = successeur(TROIS)
CINQ = successeur(QUATRE)
SIX = successeur(CINQ)
SEPT = successeur(SIX)
HUIT = successeur(SEPT)
#--- HDR --- #


def addition(a, b):
    etapes = ZERO
    somme = a
    while etapes != b:
        etapes = successeur(etapes)
        somme = successeur(somme)
    return somme


def multiplication(a, b):
    etapes = ZERO
    produit = ZERO
    while etapes != b:
        etapes = successeur(etapes)
        produit = addition(produit, a)
    return produit


def soustraction(a, b):
    difference = ZERO
    while b != a:
        if difference == a:
            raise ValueError
        difference = successeur(difference)
        b = successeur(b)
    return difference


# Tests
# un + un == deux
assert addition(UN, UN) == DEUX
# trois + cinq == huit
assert addition(TROIS, DEUX) == CINQ
# zéro * cinq == zéro
assert multiplication(ZERO, CINQ) == ZERO
# deux * trois == six
assert multiplication(DEUX, TROIS) == SIX
# cinq - trois  == deux
assert soustraction(CINQ, TROIS) == DEUX
# trois - cinq -> Opération impossible
try:
    soustraction(DEUX, CINQ)
except ValueError:
    pass
