def distance(i, j, n):
    """ Renvoie le nombre minimum de déplacements à effectuer pour se rendre
    à l'indice j en partant de l'indice i dans un tableau de longueur n """
    ...


def trier_indices(tab):
    """ Renvoie la liste des indices des nombres du tableau tab
    trié suivant l'ordre croissant des nombres du tableau tab """
    ...


def rattraper(tab):
    """ Renvoie le nombre minimal de déplacements à effectuer pour rattraper
    chaque nombre du tableau tab, dans l'ordre croissant de leur valeur """
    ...


# Tests
assert distance(0, 4, 5) == 1
assert distance(2, 3, 5) == 1
assert distance(4, 1, 5) == 2

assert trier_indices([1, 2, 3, 4]) == [0, 1, 2, 3]
assert trier_indices([4, 3, 2, 1]) == [3, 2, 1, 0]
assert trier_indices([3, 1, 2, 4, 5]) == [1, 2, 0, 3, 4]

assert rattraper([1, 2, 3, 4, 5]) == 4
assert rattraper([3, 1, 2, 4, 5]) == 6
assert rattraper([4, 3, 2, 1]) == 3
assert rattraper([5, 7, 4]) == 2
