def catalan_recursif(n: int) -> int:
    n1, n2, somme = 0, 0, 0

    if n == 0 or n == 1:
        somme = 1

    for i in range(1, n+1):
        n1 = catalan_recursif(i - 1)
        n2 = catalan_recursif(n - i)
        somme = somme + n1 * n2

    return somme
