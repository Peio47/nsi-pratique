# dicts

We've seen several types: `str`, `int`, `float`, `bool`, and `list`.
Only one of these types can contain multiple values: `list`.
Now we're going to learn about another container type: `dict`, short for ***dictionary***.

Think of the familiar kind of dictionary where you look up a word to find its definition or a translation in another language.
Dictionaries in Python are similar, but more general. You look up a *key* (e.g. a word) to get the associated *value* (e.g. a definition or translation).

For example, here's a little dictionary translating English words to French:

    french = {'apple': 'pomme', 'box': 'boite'}

Run the line above in the shell.

----

`french` is a dictionary with two key-value pairs:

- `'apple': 'pomme'` where `'apple'` is the key and `'pomme'` is the value.
- `'box': 'boite'` where `'box'` is the key and `'boite'` is the value.

Like lists, a comma (`,`) is used to separate items (key-value pairs) from each other. A colon (`:`) separates the keys from the values.
Note that curly brackets (`{}`) are used to create the dictionary instead of the square brackets (`[]`) used when writing lists.

Remember that with lists, you get values based on their *index*, i.e. their position in the list.
So if `words = ['apple', 'box']`, then `words[0]` is `'apple'` and `words[1]` is `'box'`.
Try this in the shell:

    french[0]

-----

That doesn't work because the position of items in a dictionary usually doesn't matter.
You don't usually care what's the 2nd or 5th or 100th word of the dictionary,
you just want to find a specific word like 'apple'. So try that instead:

    french['apple']

-----

That's better! Now run a similar line in the shell to look up the translation for 'box'.

    # not shown to user
    french['box']

-----

And now you know both Python and French!

Now let's translate from French to English:

    french['pomme']

-----

Sorry, you can't do that either. You can only look up a key to get its value, not the other way around.
The dictionary `french` only has 2 keys: `'apple'` and `'box'`. `'pomme'` is a value, not a key.
We'll soon learn why you can't just look up values directly, and what you can do about it.

Note that both `french[0]` and `french['pomme']` raised the same type of error: a `KeyError`.
This error means that the provided key (`0` or `'pomme'` in this case) wasn't found in the dictionary.
It's not that `french[0]` isn't *allowed*, it's just that it means the same thing as always:
find the value associated with the key `0`. In this case it finds that no such key exists.
But `0` *could* be a key, because many types of keys are allowed, including strings and numbers.

======  (end of page)

Let's see dictionaries in a real life problem. Imagine you're building an online shopping website.
You keep the prices of all your items in a dictionary:

    prices = {'apple': 2, 'box': 5, 'cat': 100, 'dog': 100}

Here you can see one reason why looking up values in a dictionary could be a problem.
What would `prices[100]` be? `'dog'`? `'cat'`? `['dog', 'cat']`?
The same value can be repeated any number of times in a dictionary.
On the other hand, keys have to be unique. Imagine if your prices started like this:

    prices = {'apple': 2, 'apple': 3, ...}

How much does an apple cost? We know it's `prices['apple']`, but is that `2` or `3`?
Clearly there should only be one price, so duplicate keys aren't allowed.

Anyway, this is a normal shop where things have one price.
This normal shop has normal customers with normal shopping lists like `['apple', 'box', 'cat']`.
And even though your customers have calculators in their pockets, they still expect you to add up all the prices
yourself and tell them how much this will all cost, because that's what normal shops do.
So let's write a function that does that. Complete the function below, particularly the line `price = ...`

    __copyable__
    def total_cost(cart, prices):
        result = 0
        for item in cart:
            price = ...
            result += price
        return result

    assert_equal(
        total_cost(
            ['apple', 'box', 'cat'],
            {'apple': 2, 'box': 5, 'cat': 100, 'dog': 100},
        ),
        107,
    )

-----

Perfect! You publish your website and start dreaming about how rich you're going to be.
But soon you get a complaint from a customer who wants to buy 5 million dogs...and 2 boxes to put them in.
Your website allows buying the same items several times, e.g. `total_cost(['box', 'box'], {...})` works,
but they have to add each item one at a time, and for some reason this customer doesn't want to click
'Add to Cart' 5 million times. People are so lazy!

Here's the new code for you to fix:

    __copyable__
    def total_cost(cart, amounts, prices):
        result = 0
        for item in cart:
            price = ...
            amount = ...
            result += price * amount
        return result

    assert_equal(
        total_cost(
            ['dog', 'box'],
            {'dog': 5000000, 'box': 2},
            {'apple': 2, 'box': 5, 'cat': 100, 'dog': 100},
        ),
        500000010,
    )

We've added another parameter called `amounts` to `total_cost`.
Now `cart` is still a list of strings, but it doesn't have any duplicates.
`amounts` is a dictionary where the keys are the items in `cart` and the corresponding values are the amount
of that item that the customer wants to buy.

-----

Not bad! But you may have noticed that it looks a bit awkward. Why do we have to specify `'dog'` and `'box'` in both the `cart` and the `amounts`?
On the next page we'll look at how to loop directly over the keys of a dictionary,
so we can get rid of the `cart` argument.

But first, let's practice what we've learned a bit more.
[Earlier in the course](#IntroducingElif) we looked at converting one strand of DNA
into a new strand with matching nucleotides:

    __copyable__
    def substitute(string):
        result = ''
        for char in string:
            if char == 'A':
                char = 'T'
            elif char == 'T':
                char = 'A'
            elif char == 'G':
                char = 'C'
            elif char == 'C':
                char = 'G'
            string += char
        return result

    original = 'AGTAGCGTCCTTAGTTACAGGATGGCTTAT'
    expected = 'TCATCGCAGGAATCAATGTCCTACCGAATA'
    assert_equal(substitute(original), expected)

Now we can use dictionaries to make this code both shorter and more general so it can be used for other purposes.
Your job is to add another argument to the `substitute` function: a dictionary called `d`.
The keys of `d` represent characters
in the first argument `string` that should be replaced by the corresponding values of `d`. For example, `'A': 'T'`
means that `A` should be replaced by `T`:

    __copyable__
    def substitute(string, d):
        ...

    original = 'AGTAGCGTCCTTAGTTACAGGATGGCTTAT'
    expected = 'TCATCGCAGGAATCAATGTCCTACCGAATA'
    assert_equal(substitute(original, {'A': 'T', 'T': 'A', 'C': 'G', 'G': 'C'}), expected)

---

Nice! Here's an example of how this function can also be used to encrypt and decrypt secret messages:

    __copyable__
    plaintext = 'helloworld'
    encrypted = 'qpeefifmez'
    letters = {'h': 'q', 'e': 'p', 'l': 'e', 'o': 'f', 'w': 'i', 'r': 'm', 'd': 'z'}
    reverse = {'q': 'h', 'p': 'e', 'e': 'l', 'f': 'o', 'i': 'w', 'm': 'r', 'z': 'd'}
    assert_equal(substitute(plaintext, letters), encrypted)
    assert_equal(substitute(encrypted, reverse), plaintext)

The same function works in both directions, we just need to pass it different dictionaries.
The two dictionaries are almost the same, we just swap around the key and value in each pair.
So to encrypt, we replace `e` with `p`, and to decrypt we change `p` back to `e`.

Note that `'e'` is both a key and a value in `letters`.
Looking up `letters['e']` means that we're asking about `'e'` as a *key*, so it gives `'p'`.
Remember, we can't use `letters` to ask which key is associated with `'e'` as a *value*.
But in this case we can use the other dictionary for that: `reverse['e']` gives `'l'`,
and `letters['l']` gives `'e'` again.
Soon you'll write a function to create a dictionary like `reverse` automatically,
i.e. `reverse = swap_keys_values(letters)`.

======= (end of page)

Copy this code into the editor:

    __copyable__
    amounts = {'apple': 1, 'cat': 10}
    print(amounts)

Then change `print(amounts)` to `print(amounts.keys())`, and run the whole program.

-----

The `.keys()` method of `dict` does basically what you'd expect. You can iterate over the value it returns
just like you'd iterate over a list:

    __copyable__
    amounts = {'apple': 1, 'cat': 10}
    for key in amounts.keys():
        print(key)

----

Actually, you don't even need `.keys()`. Iterating directly over a dictionary automatically iterates over its keys.
Sometimes it's nice to write `.keys()` to make your code more readable, but you don't have to.
Remove the `.keys()` and run the code again.

----

Now you can use this to remove the `cart` argument from our function on the previous page:

    __copyable__
    def total_cost(amounts, prices):
        result = 0
        for item in ...:
            price = prices[item]
            amount = amounts[item]
            result += price * amount
        return result

    assert_equal(
        total_cost(
            {'dog': 5000000, 'box': 2},
            {'apple': 2, 'box': 5, 'cat': 100, 'dog': 100},
        ),
        500000010,
    )

-----

That looks nice! We've fully solved the problem of adding up the total cost.

Coming back to our first example: write a function
which prints out each word in an English-to-French dictionary and its translation, labeling them with their languages.
Here's your starting code:

    __copyable__
    def print_words(french):
        ...

    print_words({'apple': 'pomme', 'box': 'boite'})

For example, the last line of code above should print:

    English: apple
    French: pomme
    ---
    English: box
    French: boite
    ---

----

Great! Now let's add a German dictionary as well:

    __copyable__
    def print_words(french, german):
        ...

    print_words(
        {'apple': 'pomme', 'box': 'boite'},
        {'apple': 'apfel', 'box': 'kasten'},
    )

That should print:

    English: apple
    French: pomme
    German: apfel
    ---
    English: box
    French: boite
    German: kasten
    ---

---

Beautiful! There's a pattern emerging here. The two languages could be merged into one big nested dictionary:

    __copyable__
    def print_words(words):
        for word in words:
            languages = words[word]
            print(f"English: {word}")
            print(f"French: {languages['French']}")
            print(f"German: {languages['German']}")
            print(f"---")
    
    
    print_words({
        'apple': {
            'French': 'pomme',
            'German': 'apfel',
        },
        'box': {
            'French': 'boite',
            'German': 'kasten',
        },
    })

======

Now we'll learn how to add key-value pairs to a dictionary,
e.g. so that we can keep track of what the customer is buying.
Before looking at dictionaries, let's remind ourselves how to add items to a list. Run this program:

    __copyable__
    cart = []
    cart.append('dog')
    cart.append('box')
    print(cart)

------

Pretty simple. We can also change the value at an index, replacing it with a different one:

    __copyable__
    cart = ['dog', 'cat']
    cart[1] = 'box'
    print(cart)

------

What if we used that idea to create our list in the first place?
We know we want a list where `cart[0]` is `'dog'` and `cart[1]` is `'box'`, so let's just say that:

    __copyable__
    cart = []
    cart[0] = 'dog'
    cart[1] = 'box'
    print(cart)

------

Sorry, that's not allowed. For lists, subscript assignment only works for existing valid indices.
But that's not true for dictionaries! Try this: 

    amounts = {}
    amounts['dog'] = 5000000
    amounts['box'] = 2
    print(amounts)

-----

That's exactly what we need. When the customer says they want 5 million boxes,
we can just put that information directly into our dictionary.

Of course, most customers just buy one dog, and they expect to be able to just click an 'Add to Cart' to do that
instead of having to type '1' on their keyboard. Even our dog-loving customer probably prefers to click a button twice
to buy two boxes. So we need something like this:

    __copyable__
    amounts = {}
    amounts['box'] = amounts['box'] + 1
    amounts['box'] = amounts['box'] + 1
    print(amounts)

------

Yet another error? Actually no, we've seen this `KeyError` before, and the reason for it is the same.
To calculate `amounts['box'] + 1`, we need to get `amounts['box']`, but it doesn't exist yet.
It'd be great if it just assumed that `amounts['box']` was `0` by default as an initial value,
but Python can't guess that for us, even in the context of adding numbers together. We need to tell it.

Keep the rest of the program above the same, but add a line `amounts['box'] = 0` after the first line `amounts = {}`,
then run it again.

    amounts = {}
    amounts['box'] = 0
    amounts['box'] = amounts['box'] + 1
    amounts['box'] = amounts['box'] + 1
    print(amounts)

-----

By the way, as before, you can shorten:

    amounts['box'] = amounts['box'] + 1

to just:

    amounts['box'] += 1

but in this case the longer version was helpful for highlighting the source of the error.

To avoid getting an error when someone adds something new to their cart, we can set an initial value of `0`
for *all* the items in the store.
Write a function which accepts the dictionary `prices` as an argument, and returns a *new* dictionary
with the same keys but which sets all the values to `0`. Don't change `prices` itself, we can't make everything free!

    __copyable__
    def make_initial_amounts(prices):
        ...
        # TODO
        amounts = {}
        for key in prices:
            amounts[key] = 0
        return amounts
    
    def test():
        prices = {'apple': 2, 'box': 5, 'cat': 100, 'dog': 100}
        assert_equal(
            make_initial_amounts(prices),
            {'apple': 0, 'box': 0, 'cat': 0, 'dog': 0}
        )
        assert_equal(
            prices,
            {'apple': 2, 'box': 5, 'cat': 100, 'dog': 100}
        )
    
    test()

-----

Well done! ... TODO

=====

cart = ['dog', 'box']
print('dog' in cart)
print('cat' in cart)

----

cart = {'dog': 0, 'box': 1}
print('dog' in cart)
print('cat' in cart)

----

cart = {'dog': 0, 'box': 1}
print(not 'dog' in cart)
print(not 'cat' in cart)

---

cart = {'dog': 0, 'box': 1}
print('dog' not in cart)
print('cat' not in cart)

---

def add_one(amounts, key):
    ...
    # TODO
    if key not in amounts:
        amounts[key] = 0
    amounts[key] += 1

def test():
    amounts = {}
    add_one(amounts, 'box')
    assert_equal(amounts, {'box': 1})
    add_one(amounts, 'box')
    assert_equal(amounts, {'box': 2})

test()

---

def add_one(amounts, key):
    if key not in amounts:
        amounts[key] = 0
    amounts[key] += 1

def add_one(amounts, key):
    if key in amounts:
        amounts[key] += 1
    else:
        amounts[key] = 1

def add_one(amounts, key):
    amounts[key] = amounts.get(key, 0) + 1

---
        
def get_amounts(cart):
    amounts = {}
    for item in cart:
        add_one(amounts, item)
    return amounts

----

def swap_keys_values(d):
    result = {}
    for key in d:
        value = d[key]
        result[value] = key
    return result

----

def group(d):
    result = {}
    for key in d:
        value = d[key]
        result[value] = result.get(value, []) + [key]
    return result

assert group({'a': 1, 'b': 2, 'c': 1}) == {1: ['a', 'c'], 2: ['b']}

---






































============

def encrypt(string: str, cipher: dict[str, str]) -> str:
    result = ''
    for char in string:
        result += cipher[char]
    return result

def decrypt(string: str, cipher: dict[str, str]) -> str:
    return encrypt(string, swap_keys_values(cipher))

def get_cipher(string: str, encrypted: str) -> typing.Union[dict[str, str], type(None)]:
    result = {}
    for i in range(len(string)):
        key = string[i]
        value = encrypted[i]
        if key in result and result[key] != value:
            return None

        result[key] = value

    return result

assert encrypt('ab', {'a': '1', 'b': '2'}) == '12'
assert swap_keys_values({'a': '1', 'b': '2'}) == {'1': 'a', '2': 'b'}
assert decrypt('12', {'a': '1', 'b': '2'}) == 'ab'
assert get_cipher('ab', '12') == {'a': '1', 'b': '2'}
assert get_cipher('aba', '121') == {'a': '1', 'b': '2'}
assert get_cipher('abb', '122') == {'a': '1', 'b': '2'}
assert get_cipher('abb', '121') == None
assert get_cipher('aba', '122') == None

def count(string: str) -> dict[str, int]:
    result = {}
    for char in string:
        result[char] = result.get(char, 0) + 1
    return result

assert count('aba') == {'a': 2, 'b': 1}

def group(d: dict[K, V]) -> dict[V, list[K]]:
    result = {}
    for key in d.keys():
        value = d[key]
        result[value] = result.get(value, []) + [key]
    return result

assert group({'a': 1, 'b': 2, 'c': 1}) == {1: ['a', 'c'], 2: ['b']}

def max_key(d: dict[K, V]) -> K:
    max_value = max(d.values())
    return swap_keys_values(d)[max_value]

def max_key(d: dict[K, V]) -> K:
    max_value = 0
    for key in d.keys():
        value = d[key]
        if value > max_value:
            result = key
            max_value = value
    return result

assert max_key({'a': 1, 'b': 2}) == 'b'
assert max_key({'a': 2, 'b': 1}) == 'a'

def total_cost(cart: list[str], prices: dict[str, float]) -> float:
    result = 0
    for item in cart:
        price = prices[item]
        result += price
    return result


def total_cost(to_buy: dict[str, str], shops: dict[str, dict[str, float]]) -> float:
    result = 0
    for key in cart.keys():
        shop = to_buy[key]
        prices = shops[shop]
        price = prices[key]
        result += price
    return result


def find_shops(to_buy: list[K], shops: dict[V, dict[K, float]]) -> dict[K, V]:
    result = {}
    for key in to_buy:
        for shop_name in shops.keys():
            prices = shops[shop_name]
            if key in prices:
                result[key] = shop_name
    return result


def best_prices(to_buy: list[K], shops: dict[V, dict[K, float]]) -> dict[K, V]:
    result = {}
    for key in to_buy:
        min_price = None
        for shop_name in shops.keys():
            prices = shops[shop_name]
            price = prices[key]
            if min_price is None or price < min_price:
                min_price = price
                result[key] = shop_name
    return result

def items_to_shops(shops: dict[V, dict[K, float]]) -> dict[K, dict[V, float]]:
    result = {}
    for shop_name in shops.keys():
        prices = shops[shop_name]
        for item in prices.keys():
            price = prices[item]
            item_dict = result.get(item, {})
            result[item] = item_dict
            item_dict[shop_name] = price
    return result


def best_prices(to_buy: list[K], shops: dict[V, dict[K, float]]) -> dict[K, V]:
    inverted = items_to_shops(shops)
    result = {}
    for key in to_buy:
        prices = inverted[key]
        price_to_shop = swap_keys_values(prices)
        best_price = min(prices.values())
        best_shop = price_to_shop[best_price]
        result[key] = best_shop
    return result
